# -*- coding: utf-8 -*-
"""Main Controller"""
from tg import expose, flash, require, url, lurl
from tg import request, redirect,session, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from webapp.lib.base import BaseController
from webapp.controllers.error import ErrorController
from webapp.controllers.AccountsController import AccountsController 
from webapp.lib.app_globals import Globals
from beaker.session import Session

__all__ = ['RootController']




class RootController(BaseController):
    """
    The root controller for the webapp application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    error = ErrorController()
    
    Account= AccountsController()
    

    def _before(self, *args, **kw):
        tmpl_context.project_name = "webapp"

    @expose('index.jinja')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('info.jinja')
    def info(self):
        return dict(page='info')
    
    @expose('existingorg.jinja')
    def existingorg(self):
        orgs = Globals.sp.getOrganisationNames()
        return dict(orgs = orgs)
    
    @expose('neworg.jinja')
    def neworg(self):
        return dict(page='neworg')
    
    @expose('orgdetail.jinja')
    def orgdetail(self, **kw):
        orgParams = []
        orgParams.append(kw['orgname'])
        orgParams.append(kw['from_day']+"-"+kw['from_month']+"-"+kw['from_year'])
        orgParams.append(kw['to_day']+"-"+kw['to_month']+"-"+kw['to_year'])
        orgParams.append(kw['orgtype'])
        return dict(params=orgParams)
    
    @expose('json')
    def getFinancialYears(self, **kw):
        print kw
        financialyears = Globals.sp.getFinancialYear(kw['orgname'])
        return dict(years= financialyears)
    
    @expose('login.jinja')
    def login(self, **kw):
        if(kw['subbutton']=="skip"):
            params=[kw['orgname'],kw['fromDate'],kw['toDate'],kw['orgtype']]
            print params
            deploy=Globals.sp.Deploy(params)
            session['gnukhata']=deploy[1]
            session['orgname']=params[0]
            session['financialFrom']=params[1]
            session['financialTo']=params[2]
            session['orgtype']=params[3]
            session.save()
            setorg = Globals.sp.organisation.setOrganisation([session['orgtype'],session['orgname'],"","","","","","","","","","","","","","","","",""],session['gnukhata'])
            print setorg
            return dict(id=deploy[1])
        elif(kw['subbutton']=="save"):
            params=[kw['orgname'],kw['fromDate'],kw['toDate'],kw['orgtype']]
            deploy=Globals.sp.Deploy(params)
            session['gnukhata']=deploy[1]
            session['orgname']=params[0]
            session['financialFrom']=params[1]
            session['financialTo']=params[2]
            session['orgtype']=params[3]
            session.save()
            par=[]
            par.append(kw['orgtype'])
            par.append(kw['orgname'])
            par.append(kw['add'])
            par.append(kw['ct'])
            par.append(kw['pcode'])
            par.append(kw['st'])
            par.append(kw['cntr'])
            par.append(kw['tel'])
            par.append(kw['fax'])
            par.append(kw['wbsite'])
            par.append(kw['emailid'])
            par.append(kw['pmntaccno'])
            par.append(kw['vatno'])
            par.append(kw['srvctaxno'])
            par.append(kw['reg'])
            par.append(kw['dtregdd']+"-"+kw['dtregmm']+"-"+ kw['dtregyy'])
            par.append(kw['fcraregname'])
            par.append(kw['dtfcraregdd']+ "-"+kw['dtfcraregmm']+"-"+ kw['dtfcraregyy'])
            par.append("0")
            print par,session['gnukhata']
            setORg=Globals.sp.organisation.setOrganisation(par,session['gnukhata'])
            print setORg
            return dict(id=deploy[1])
    
    @expose('login.jinja')
    def loginAfterExistingForm(self, **kw):
        orgname=kw['orgname']
        year = kw['financialYear']
        Year = year.split(",")
        params = [orgname,Year[0],Year[1]]
        client_id = Globals.sp.getConnection(params)
        session['gnukhata']=client_id
        session['orgname']=orgname
        session['financialFrom']=Year[0]
        session['financialTo']=Year[1]
        isngo=Globals.sp.organisation.getOrganizationType(client_id)
        print isngo
        if(isngo):
            session['orgtype']="ngo"
        else:
            session['orgtype']="profit making"
        session.save()
        print client_id
        return dict(id=client_id)
    
    
    
